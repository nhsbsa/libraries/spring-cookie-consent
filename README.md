### NHS Spring boot cookie consent 

This project has taken existing development from    
[nhsuk/cookie-consent](https://github.com/nhsuk/cookie-consent) 
which is a node.js project and converted principal to work within a spring boot application. 
The library is driven by thymeleaf and javaScript and has one controller 
which is used for testing purposes only   


Add dependency:

```xml    
 <dependency>
  <artifactId>spring-cookie-consent</artifactId>
  <groupId>uk.nhs.nhsbsa.prescriptions</groupId>
  <version>1.0.0</version>
</dependency>
```

A sample index.html can be reviewed to see how you could adopt the calls into existing code:

```html
<!DOCTYPE html>
<!--[if lt IE 9]>
<html xmlns:th="http://www.thymeleaf.org" class="lte-ie8" lang="en"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en" xmlns:sec="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">
<!--<![endif]-->

<head th:replace="~{base :: head}"></head>

<body>

<th:block th:insert="~{base :: skiplink}"></th:block>
<header th:replace="~{base :: header}"></header>

<!-- Cookie Consent plugin css fonts files by default this is loaded in by nhs toolkit,
if your project does not have nhs toolkit then enable this feature -->
<!-- disabled segment
<th:block
    th:replace="~{cookie-consent-base::fonts}"></th:block>
-->

<!-- Cookie Consent plugin css  files, should be loaded by header component-->
<th:block
        th:replace="~{cookie-consent-base::head-links}"></th:block>

<!-- Cookie Consent plugin js files, should be loaded by header component.,
 configurations: 
 enableSplunk is false by default - if enabled it will log choice to user cookie choice to splunk
 noBanner is false by default - if enabled it will hide cookie banner
 --- 
 --> 
<th:block
        th:replace="~{cookie-consent-base::scripts(enableSplunk=false, noBanner=false)}"></th:block>
<!-- You could simplify above to this
 <th:block
        th:replace="~{cookie-consent-base::scripts}"></th:block>
-->


<div class="nhsuk-width-container">
  <main class="nhsuk-main-wrapper" id="maincontent" role="main">

    <div class="nhsuk-grid-row">
      <div class="nhsuk-grid-column-full">
        <th:block
            th:insert="~{nhsbsa-design-system/components :: heading(
            id='dashboard', heading='Dashboard')}">
        </th:block>
      </div>
    </div>

   
    
    
    <!-- Cookie Consent plugin actual banner includes policyUrl,
    if policyUrl is not defined project will refer to default nhs policy url.
    This is a hidden component that needs to be declared in main heading around where navigation is
     created within site main templates
    -->
     
   <th:block
           th:replace="~{cookie-consent-base::
           banner(policyUrl='www')}"></th:block>
    
</main>
</div>

</body>
</html>

```



This is similar to above example but includes a more detailed revision with additional 
essential cookies that are not controlled by cookie-consent process 
There are references to 4 js files but there is only 1 requirement 
`/webjars/spring-cookie-consent/javascript/main.js`, 
which is what controls the cookie-consent process

## Optional js files  
> necessary.js
```javascript
      document.cookie = 'necessary=This is a necessary cookie;path=/';
```

> statistics.js
```javascript
     document.cookie = 'statistics=This is a statistics cookie';
```
> show-cookie-data.js 

This can be found within plugin and simply lists out current cookies  

```html
<!DOCTYPE html>
<!--[if lt IE 9]>
<html xmlns:th="http://www.thymeleaf.org" class="lte-ie8" lang="en"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en" xmlns:sec="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">
<!--<![endif]-->

<head th:replace="~{base :: head}"></head>

<body>

<th:block th:insert="~{base :: skiplink}"></th:block>
<header th:replace="~{base :: header}"></header>

<!-- Cookie Consent plugin css fonts files by default this is loaded in by nhs toolkit,
if your project does not have nhs toolkit then enable this feature -->

<th:block
    th:replace="~{cookie-consent-base::fonts}"></th:block>


<!-- Cookie Consent plugin css  files, should be loaded by header component-->
<th:block
        th:replace="~{cookie-consent-base::head-links}"></th:block>

<!-- Cookie Consent plugin js files, should be loaded by header component-->
<!-- disabled using actual js file in this example
<th:block
        th:replace="~{cookie-consent-base::scripts}"></th:block>
-->

<!-- Cookie Consent plugin -- by default data-nobanner is false 
no declaration required if set to true it will hide banner 
data-enable-splunk by default is false can be set by a variable like so 
-->
<script th:src="@{/webjars/spring-cookie-consent/javascript/main.js}"
        th:attr="data-enable-splunk=${true},data-nobanner=false"
        type="text/javascript"></script>

        
<div class="nhsuk-width-container">
  <main class="nhsuk-main-wrapper" id="maincontent" role="main">

    <div class="nhsuk-grid-row">
      <div class="nhsuk-grid-column-full">
        <th:block
            th:insert="~{nhsbsa-design-system/components :: heading(
            id='dashboard', heading='Dashboard',
            subHeading='')}">
        </th:block>
      </div>
    </div>

    <link rel="stylesheet" th:href="@{/webjars/spring-cookie-consent/stylesheets/style.css}"/>

    <!-- Necessary cookies should always be set regardless of user preferences  -->
    <script th:src="@{/webjars/spring-cookie-consent/javascript/test/necessary.js}"></script>


      <!-- Statistics cookies have to be accepted before they are set -->
    <script th:src="@{/webjars/spring-cookie-consent/javascript/test/statistics.js}" 
            data-cookieconsent="statistics" type="text/plain">
    </script>
      
   <!-- Cookie Consent plugin -- banner content   (hidden by default) used by js call below -->
   <th:block
           th:replace="~{cookie-consent-base::banner(policyUrl='www')}"></th:block>
   type="text/javascript"></script>



<script data-cookieconsent="statistics" type="text/plain">
  document.cookie = 'inline-js=this cookie is from inline javascript';
</script>

<h1>Cookies:</h1>
<button id="cookie-list-refresh">Refresh</button>
<div id="cookie-list"></div>

<iframe data-src="https://example.com" data-cookieconsent="statistics"></iframe>

<script th:src="@{/webjars/spring-cookie-consent/javascript/test/show-cookie-data.js}" ></script>
</main>
</div>

</body>
</html>

```


### Testing cookie consent within plugin:

Create a component scan file like below in your application

```java
package uk.nhs.nhsbsa.prescriptions.dpc.config;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("uk.nhs.nhsbsa.prescriptions.spring.cookie.consent")
public class ConsentConfig {

}
```

With plugin installed you should then be able to hit `https://localhost:{port}/{app}/cookie-consent/test` 
in this specific example `https://localhost:8084/dpc/cookie-consent/test`

-----
