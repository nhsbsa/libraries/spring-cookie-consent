package uk.nhs.nhsbsa.prescriptions.spring.cookie.consent.controller.test;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/cookie-consent")
public class CookieConsentExampleController {

  @GetMapping("/test")
  public String test() {
    return "cookie-consent-example/test";
  }

}
