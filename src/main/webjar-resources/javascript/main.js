
//--------------------------------------------------------------------------
// main.js
//--------------------------------------------------------------------------

function getPreferences() {
  return getConsentSetting('preferences');
}

function getStatistics() {
  return getConsentSetting('statistics');
}

function getMarketing() {
  return getConsentSetting('marketing');
}

function getConsented() {
  return getConsentSetting('consented');
}

function setPreferences(value) {
  setConsentSetting('preferences', value);
}

function setStatistics(value) {
  setConsentSetting('statistics', value);
}

function setMarketing(value) {
  setConsentSetting('marketing', value);
}

function setConsented(value) {
  setConsentSetting('consented', value);
}

window.addEventListener('DOMContentLoaded', onload);

//--------------------------------------------------------------------------
// cookieconsent.js
//--------------------------------------------------------------------------
/**
 * If cookie rules/regulations change and the cookie itself needs to change,
 * bump this version up afterwards. It will then give the user the banner again
 * to consent to the new rules
 */
const COOKIE_VERSION = 4;
const COOKIE_NAME = 'nhsuk-cookie-consent';

/**
 * enum for different types of cookie.
 * LONG - a long lasting cookie
 * SESSION - a cookie that should expire after the current session
 */
const COOKIE_TYPE = {
  LONG: 'long',
  SESSION: 'session',
};

/*
 * NO_BANNER mode means that the banner will never be shown, and users will
 * have all cookie-types activated.
 * We need this switch to be able to totally disable the functionality of this
 * cookie-consent library for a co-ordinated cross-platform release.
 */
//const NO_BANNER = (process.env.NO_BANNER === 'true');
const NO_BANNER = false; //(process.env.NO_BANNER === 'true');

/* eslint-disable sort-keys */
// Pre-defined cookie types in line with cookiebot categories
const defaultConsent = {
  necessary: true,
  preferences: false,
  statistics: false,
  marketing: false,
  consented: false,
};
/* eslint-enable sort-key */

/**
 * Get the consent cookie and parse it into an object
 */
function getCookie() {
  const rawCookie = getRawCookie(COOKIE_NAME);
  return JSON.parse(rawCookie);
}

/**
 * Set the consent cookie, turning the value object into a string
 * Creates a new cookie or replaces a cookie if one exists with the same name
 */
function createCookie(value, days, path, domain, secure) {
  const stringValue = JSON.stringify(value);
  return createRawCookie(COOKIE_NAME, stringValue, days, path, domain, secure);
}

/**
 * returns an object containing consent boolean values for each consent type
 */
function getConsent() {
  const cookieValue = getCookie();
  if (!cookieValue) {
    return {};
  }
  delete cookieValue.version;
  return cookieValue;
}

/**
 * Create the consent cookie.
 * `consent` is an object of key/boolean pairs
 * e.g { marketing: false, statistics: true }
 *
 * `mode` is a COOKIE_TYPE const e.g COOKIE_TYPE.SESSION.
 * Defaults to COOKIE_TYPE.LONG
 *
 * This function will respect any consent settings that already exist in a cookie,
 * using keys defined in the `consent` object to overwrite the consent.
 */
function setConsent(consent, mode = COOKIE_TYPE.LONG) {
  const path = '/';

  let days;
  // default cookie mode is COOKIE_TYPE.LONG
  if (mode === COOKIE_TYPE.LONG) {
    // default based on ICO guidance
    days = 90;
  } else if (mode === COOKIE_TYPE.SESSION || !mode) {
    days = null;
  } else {
    // cookie mode not recognised
    throw new Error(`Cookie mode ${mode} not recognised`);
  }

  const existingConsent = getConsent();

  const cookieValue = {
    // merge the consent that already exists with the new consent setting
    ...existingConsent,
    ...consent,
    // add version information to the cookie consent settings
    version: COOKIE_VERSION,
  };

  createCookie(cookieValue, days, path);
}

/**
 * Get the cookie version that is currently set on the browser.
 * Returns integer, or null if no cookie is set.
 */
function getUserCookieVersion() {
  const cookie = getCookie();
  return cookie === null ? null : cookie.version;
}

/**
 * Is the cookie that is currently set on the browser valid.
 * a "valid" cookie is one which has the latest COOKIE_VERSION number.
 * Returns true/false if a cookie is found on the browser.
 * Returns null if no cookie is found.
 */
function isValidVersion() {
  const currentVersion = getUserCookieVersion();
  return currentVersion === null ? null : currentVersion >= COOKIE_VERSION;
}

function getConsentSetting(key) {
  const cookie = getConsent();
  // double ! to convert truthy/falsy values into true/false
  return !!cookie[key];
}

function setConsentSetting(key, value) {
  if (!value) {
    deleteCookies();
  }
  // double ! to convert truthy/falsy values into true/false
  setConsent({ [key]: !!value });
}

function enableScriptsAndIframes() {
  const allCategories = ['preferences', 'statistics', 'marketing'];
  // Filter out categories that do not have user consent
  const allowedCategories = allCategories.filter(category => getConsentSetting(category) === true);

  enableScriptsByCategories(allowedCategories);
  enableIframesByCategories(allowedCategories);
}

// If consent is given, change the value of the cookie
function acceptConsent() {
  setConsent({
    ...defaultConsent,
    consented: true,
  });
}

// If analytics consent is given, change the value of the cookie
function acceptAnalyticsConsent() {
  setConsent({
    statistics: true,
    consented: true,
  });
  enableScriptsAndIframes();
}

/**
 * Hit a URL set up to monitor logs in Splunk
 * @param {string} route route to hit for logging
 */
function hitLoggingUrl(route) {
  let hitSplunk = false;

  if (scriptTag && scriptTag.getAttribute('data-enable-splunk')) {
    hitSplunk = scriptTag.getAttribute('data-enable-splunk');
  }

  if (hitSplunk ===  true) {
    const oReq = new XMLHttpRequest();
    oReq.open(
        'GET',
        `https://www.nhs.uk/our-policies/cookies-policy/?policy-action=${route}`
    );
    oReq.send();
  }
}

/**
 * Should the banner be shown to a user?
 * Returns true or false
 */
function shouldShowBanner() {
  // If the `nobanner` setting is used, never show the banner.
  if (getNoBanner()) {
    return false;
  }

  // Do not show the banner if we are on the policy page.
  if (document.location.href === makeUrlAbsolute(getPolicyUrl())) {
    return false;
  }

  // Show the banner if there is no cookie. This user is a first-time visitor
  if (getCookie() === null) {
    return true;
  }

  // Show the banner if the user has consented before, but on an old version
  if (!isValidVersion(COOKIE_VERSION)) {
    return true;
  }

  // Show the banner if the user has a cookie, but didn't actively consent.
  // For example, they didn't interact with the banner on a previous visit.
  if (getConsentSetting('consented') === false) {
    return true;
  }

  return false;
}

/*
 * function that needs to fire when every page loads.
 * - shows the cookie banner
 * - sets default consent
 * - enables scripts and iframes depending on the consent
 */
function onload() {
  if (shouldShowBanner()) {
    if (NO_BANNER) {
      // If NO_BANNER mode, we need to set "implied consent" to every cookie type
      setConsent({
            necessary: true,
            preferences: true,
            statistics: true,
            marketing: true,
            consented: false,
          },
          COOKIE_TYPE.LONG);
    } else {
      insertCookieBanner(acceptConsent, acceptAnalyticsConsent, hitLoggingUrl);
    }
  }

  // if a cookie is set but it's invalid, clear all cookies.
  if (isValidVersion(COOKIE_VERSION) === false) {
    deleteCookies();
  }

  // If there isn't a valid user cookie, create one with default consent
  if (isValidVersion() !== true) {
    setConsent(defaultConsent, COOKIE_TYPE.SESSION);
  }

  enableScriptsAndIframes();
}


//--------------------------------------------------------------------------
// cookies.js
//--------------------------------------------------------------------------

// used to create a new cookie for the user which covers different cookie types
function createRawCookie(name, value, days, path, domain, secure){
  // if number of days is given, sets expiry time
  let expires;
  if (days) {
    const date = new Date();
    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
    expires = date.toUTCString();
  } else {
    expires = '';
  }

  // appends name to cookie, making it searchable
  let cookieString = `${name}=${escape(value)}`;

  if (expires) {
    cookieString += ';expires=' + expires;
  }

  if (path) {
    cookieString += ';path=' + escape(path);
  }

  // In the cookie spec, domains must have a '.' so e.g `localhost` is not valid
  // and should never be set as the domain.
  if (domain && domain.indexOf('.') !== -1) {
    cookieString += ';domain=' + escape(domain);
  }

  if (secure) {
    cookieString += ';secure';
  }

  cookieString += ';';

  // cookiestring now contains all necessary details and is turned into a cookie
  document.cookie = cookieString;
}

/*
 * Get all cookies from document.cookie and parse them.
 * Returns an object of key/value pairs
 */
function getAllCookies() {
  const cookiesString = document.cookie || '';
  const cookiesArray = cookiesString.split(';')
  .filter(cookie => cookie !== '')
  .map(cookie => cookie.trim());

  // Turn the cookie array into an object of key/value pairs
  const cookies = cookiesArray.reduce((acc, currentValue) => {
    const [key, value] = currentValue.split('=');
    const decodedValue = decodeURIComponent(value); // URI decoding
    acc[key] = decodedValue; // Assign the value to the object
    return acc;
  }, {});

  return cookies || {};
}

/*
 * Get a cookie value by name
 */
function getRawCookie(name) {
  const cookies = getAllCookies();
  return cookies[name] || null;
}

/*
 * Remove all cookies other than nhsuk-cookie-consent cookie
 */
function deleteCookies() {
  const cookies = getAllCookies();
  const cookieNames = Object.keys(cookies);
  // We want to delete all cookies except for our consent cookie
  const cookieNamesToDelete = cookieNames.filter(name => name !== 'nhsuk-cookie-consent');

  // generate a list of domains that the cookie could possibly belong to
  const domainParts = window.location.hostname.split('.');
  const domains = domainParts.map((domainPart, i) => { // eslint-disable-line arrow-body-style
    return domainParts.slice(i).join('.');
  });

  // generate a list of paths that the cookie could possibly belong to
  const pathname = window.location.pathname.replace(/\/$/, ''); // strip trailing slash
  const pathParts = pathname.split('/');
  const paths = pathParts.map((pathPart, i) => { // eslint-disable-line arrow-body-style
    return pathParts.slice(0, i + 1).join('/') || '/';
  });

  // Loop over every combination of path and domain for each cookie we want to delete
  cookieNamesToDelete.forEach((cookieName) => {
    paths.forEach((path) => {
      domains.forEach((domain) => {
        createRawCookie(cookieName, '', -1, path, domain);
      });
    });
  });
}

//--------------------------------------------------------------------------
// banner.js
//--------------------------------------------------------------------------
function hideCookieBanner() {
  document.getElementById('cookiebanner').style.display = 'none';
}

function showCookieConfirmation() {
  document.getElementById('nhsuk-cookie-confirmation-banner').style.display = 'block';
}

function addFocusCookieConfirmation() {
  const cookieConfirmationMessage = document.getElementById('nhsuk-success-banner__message');
  cookieConfirmationMessage.setAttribute('tabIndex', '-1');
  cookieConfirmationMessage.focus();
}

function removeFocusCookieConfirmation() {
  const cookieConfirmationMessage = document.getElementById('nhsuk-success-banner__message');
  cookieConfirmationMessage.addEventListener('blur', () => {
    cookieConfirmationMessage.removeAttribute('tabIndex');
  });
}

/**
 * Call common methods on link click as well as consent type callback
 * @param {function} consentCallback callback to be called based on which link has been clicked.
 */
function handleLinkClick(consentCallback) {
  hideCookieBanner();
  consentCallback();
  showCookieConfirmation();
  addFocusCookieConfirmation();
  removeFocusCookieConfirmation();
}

/**
 * Insert the cookie banner at the top of a page.
 * @param {function} onAccept callback that is called when necessary consent is accepted.
 * @param {function} onAnalyticsAccept callback that is called analytics consent is accepted.
 * @param {function} hitLoggingUrl function that is makes request to logging URL.
 */
function insertCookieBanner(onAccept, onAnalyticsAccept, hitLoggingUrl) {
  // add a css block to the inserted html
  const div = document.createElement('div');
  var cookieBanner = document.getElementById('nhsuk-cookie-banner-container');
  if (cookieBanner !== undefined ) {
    div.innerHTML =  document.getElementById('nhsuk-cookie-banner-container').innerHTML;
  }
  /**
   * Manually insert html + css
   * else {
   *     div.innerHTML = '<div id="nhsuk-cookie-banner" data-nosnippet="true">\n  <div class="nhsuk-cookie-banner" id="cookiebanner">\n    <div class="nhsuk-width-container"> \n        <h2>Cookies on the NHS website</h2> \n        <p>We\'ve put some small files called cookies on your device to make our site work.</p>\n        <p>We\'d also like to use analytics cookies. These collect feedback and send information about how our site is used to services called Adobe Analytics, Qualtrics Feedback, Microsoft Clarity and Google Analytics. We use this information to improve our site.</p>\n        <p>Let us know if this is OK. We\'ll use a cookie to save your choice. You can <a id="nhsuk-cookie-banner__link" href="' +
   *         getPolicyUrl() +
   *         '">read more about our cookies</a> before you choose.</p>\n        <ul> \n            <li><button class=\'nhsuk-button\' id="nhsuk-cookie-banner__link_accept_analytics" href="#">I\'m OK with analytics cookies</button></li>\n            <li><button class=\'nhsuk-button\' id="nhsuk-cookie-banner__link_accept" href="#">Do not use analytics cookies</button></li>\n        </ul>\n    </div>\n  </div>\n\n  <div class="nhsuk-success-banner" id="nhsuk-cookie-confirmation-banner" style="display:none;">\n    <div class="nhsuk-width-container">\n      <p id="nhsuk-success-banner__message">You can change your cookie settings at any time using our <a href="' +
   *         getPolicyUrl() +
   *         '">cookies page</a>.</p>\n    </div>\n  </div>\n</div>\n';
   *
   *     div.innerHTML += '<style>@font-face{font-family:\'Frutiger W01\';font-display:swap;font-style:normal;font-weight:400;src:url("https://assets.nhs.uk/fonts/FrutigerLTW01-55Roman.eot?#iefix");src:url("https://assets.nhs.uk/fonts/FrutigerLTW01-55Roman.eot?#iefix") format("eot"),url("https://assets.nhs.uk/fonts/FrutigerLTW01-55Roman.woff2") format("woff2"),url("https://assets.nhs.uk/fonts/FrutigerLTW01-55Roman.woff") format("woff"),url("https://assets.nhs.uk/fonts/FrutigerLTW01-55Roman.ttf") format("truetype"),url("https://assets.nhs.uk/fonts/FrutigerLTW01-55Roman.svg#7def0e34-f28d-434f-b2ec-472bde847115") format("svg")}@font-face{font-family:\'Frutiger W01\';font-display:swap;font-style:normal;font-weight:600;src:url("https://assets.nhs.uk/fonts/FrutigerLTW01-65Bold.eot?#iefix");src:url("https://assets.nhs.uk/fonts/FrutigerLTW01-65Bold.eot?#iefix") format("eot"),url("https://assets.nhs.uk/fonts/FrutigerLTW01-65Bold.woff2") format("woff2"),url("https://assets.nhs.uk/fonts/FrutigerLTW01-65Bold.woff") format("woff"),url("https://assets.nhs.uk/fonts/FrutigerLTW01-65Bold.ttf") format("truetype"),url("https://assets.nhs.uk/fonts/FrutigerLTW01-65Bold.svg#eae74276-dd78-47e4-9b27-dac81c3411ca") format("svg")}#nhsuk-cookie-banner{-moz-osx-font-smoothing:grayscale;-webkit-font-smoothing:antialiased;font-family:Frutiger W01,Arial,Sans-serif;color:#212b32}#nhsuk-cookie-banner a{color:#005eb8}#nhsuk-cookie-banner a:visited{color:#330072}#nhsuk-cookie-banner a:hover{color:#7C2855;text-decoration:none}#nhsuk-cookie-banner a:focus{background-color:#ffeb3b;box-shadow:0 -2px #ffeb3b,0 4px #212b32;color:#212b32;outline:4px solid transparent;text-decoration:none}#nhsuk-cookie-banner a:focus:hover{text-decoration:none}#nhsuk-cookie-banner a:focus:visited{color:#212b32}#nhsuk-cookie-banner a:focus .nhsuk-icon{fill:#212b32}#nhsuk-cookie-banner a:active{color:#002f5c}@media print{#nhsuk-cookie-banner a:after{color:#212b32;content:" (Link: " attr(href) ")";font-size:14pt}}#nhsuk-cookie-banner .ie8 a:focus{outline:1px dotted #212b32}#nhsuk-cookie-banner .nhsuk-width-container{margin:0 16px;max-width:960px}@media (min-width: 48.0625em){#nhsuk-cookie-banner .nhsuk-width-container{margin:0 32px}}@media (min-width: 1024px){#nhsuk-cookie-banner .nhsuk-width-container{margin:0 auto}}#nhsuk-cookie-banner .nhsuk-width-container-fluid{margin:0 16px;max-width:100%}@media (min-width: 48.0625em){#nhsuk-cookie-banner .nhsuk-width-container-fluid{margin:0 32px}}#nhsuk-cookie-banner .nhsuk-button{font-weight:400;font-size:16px;font-size:1rem;line-height:1.5;margin-bottom:28px;-webkit-appearance:none;background-color:#007f3b;border:2px solid transparent;border-radius:4px;box-shadow:0 4px 0 #00401e;box-sizing:border-box;color:#fff;cursor:pointer;display:inline-block;font-weight:600;margin-top:0;padding:12px 16px;position:relative;text-align:center;vertical-align:top;width:auto}@media (min-width: 40.0625em){#nhsuk-cookie-banner .nhsuk-button{font-size:19px;font-size:1.1875rem;line-height:1.47368}}@media print{#nhsuk-cookie-banner .nhsuk-button{font-size:14pt;line-height:1.15}}@media (min-width: 40.0625em){#nhsuk-cookie-banner .nhsuk-button{margin-bottom:36px}}@media (max-width: 40.0525em){#nhsuk-cookie-banner .nhsuk-button{padding:8px 16px}}#nhsuk-cookie-banner .nhsuk-button:link,#nhsuk-cookie-banner .nhsuk-button:visited,#nhsuk-cookie-banner .nhsuk-button:active,#nhsuk-cookie-banner .nhsuk-button:hover{color:#fff;text-decoration:none}#nhsuk-cookie-banner .nhsuk-button::-moz-focus-inner{border:0;padding:0}#nhsuk-cookie-banner .nhsuk-button:hover{background-color:#00662f}#nhsuk-cookie-banner .nhsuk-button:focus{background:#ffeb3b;box-shadow:0 4px 0 #212b32;color:#212b32;outline:4px solid transparent}#nhsuk-cookie-banner .nhsuk-button:focus:visited{color:#212b32}#nhsuk-cookie-banner .nhsuk-button:focus:visited:active{color:#fff}#nhsuk-cookie-banner .nhsuk-button:active{background:#00401e;box-shadow:none;color:#fff;top:4px}#nhsuk-cookie-banner .nhsuk-button::before{background:transparent;bottom:-6px;content:\'\';display:block;left:-2px;position:absolute;right:-2px;top:-2px}#nhsuk-cookie-banner .nhsuk-button:active::before{top:-6px}#nhsuk-cookie-banner .nhsuk-button--secondary{background-color:#4c6272;box-shadow:0 4px 0 #263139}#nhsuk-cookie-banner .nhsuk-button--secondary:hover{background-color:#384853}#nhsuk-cookie-banner .nhsuk-button--secondary:focus{background:#ffeb3b;box-shadow:0 4px 0 #212b32;color:#212b32;outline:4px solid transparent}#nhsuk-cookie-banner .nhsuk-button--secondary:active{background:#263139;box-shadow:none;color:#fff;top:4px}#nhsuk-cookie-banner .nhsuk-button--secondary.nhsuk-button--disabled{background-color:#4c6272}#nhsuk-cookie-banner .nhsuk-button--reverse{background-color:#fff;box-shadow:0 4px 0 #212b32;color:#212b32}#nhsuk-cookie-banner .nhsuk-button--reverse:hover{background-color:#f2f2f2;color:#212b32}#nhsuk-cookie-banner .nhsuk-button--reverse:focus{background:#ffeb3b;box-shadow:0 4px 0 #212b32;color:#212b32;outline:4px solid transparent}#nhsuk-cookie-banner .nhsuk-button--reverse:active{background:#212b32;box-shadow:none;color:#fff;top:4px}#nhsuk-cookie-banner .nhsuk-button--reverse:link{color:#212b32}#nhsuk-cookie-banner .nhsuk-button--reverse:link:active{color:#fff}#nhsuk-cookie-banner .nhsuk-button--reverse.nhsuk-button--disabled{background-color:#fff}#nhsuk-cookie-banner .nhsuk-button--reverse.nhsuk-button--disabled:focus{background-color:#fff}#nhsuk-cookie-banner .nhsuk-button--disabled,#nhsuk-cookie-banner .nhsuk-button[disabled="disabled"],#nhsuk-cookie-banner .nhsuk-button[disabled]{background-color:#007f3b;opacity:0.5;pointer-events:none}#nhsuk-cookie-banner .nhsuk-button--disabled:hover,#nhsuk-cookie-banner .nhsuk-button[disabled="disabled"]:hover,#nhsuk-cookie-banner .nhsuk-button[disabled]:hover{background-color:#007f3b;cursor:default}#nhsuk-cookie-banner .nhsuk-button--disabled:focus,#nhsuk-cookie-banner .nhsuk-button[disabled="disabled"]:focus,#nhsuk-cookie-banner .nhsuk-button[disabled]:focus{background-color:#007f3b;outline:none}#nhsuk-cookie-banner .nhsuk-button--disabled:active,#nhsuk-cookie-banner .nhsuk-button[disabled="disabled"]:active,#nhsuk-cookie-banner .nhsuk-button[disabled]:active{box-shadow:0 4px 0 #00401e;top:0}#nhsuk-cookie-banner .nhsuk-button--secondary[disabled="disabled"],#nhsuk-cookie-banner .nhsuk-button--secondary[disabled]{background-color:#4c6272;opacity:0.5}#nhsuk-cookie-banner .nhsuk-button--secondary[disabled="disabled"]:hover,#nhsuk-cookie-banner .nhsuk-button--secondary[disabled]:hover{background-color:#4c6272;cursor:default}#nhsuk-cookie-banner .nhsuk-button--secondary[disabled="disabled"]:focus,#nhsuk-cookie-banner .nhsuk-button--secondary[disabled]:focus{outline:none}#nhsuk-cookie-banner .nhsuk-button--secondary[disabled="disabled"]:active,#nhsuk-cookie-banner .nhsuk-button--secondary[disabled]:active{box-shadow:0 4px 0 #263139;top:0}#nhsuk-cookie-banner .nhsuk-button--reverse[disabled="disabled"],#nhsuk-cookie-banner .nhsuk-button--reverse[disabled]{background-color:#fff;opacity:0.5}#nhsuk-cookie-banner .nhsuk-button--reverse[disabled="disabled"]:hover,#nhsuk-cookie-banner .nhsuk-button--reverse[disabled]:hover{background-color:#fff;cursor:default}#nhsuk-cookie-banner .nhsuk-button--reverse[disabled="disabled"]:focus,#nhsuk-cookie-banner .nhsuk-button--reverse[disabled]:focus{outline:none}#nhsuk-cookie-banner .nhsuk-button--reverse[disabled="disabled"]:active,#nhsuk-cookie-banner .nhsuk-button--reverse[disabled]:active{box-shadow:0 4px 0 #212b32;top:0}#nhsuk-cookie-banner .ie8 .nhsuk-button:focus{outline:1px dotted #212b32}#nhsuk-cookie-banner .nhsuk-cookie-banner{background:white;position:relative;box-shadow:0 0 4px 0 #212b32;padding:24px 0 19px;width:100%;z-index:1}#nhsuk-cookie-banner h2{margin-bottom:16px;display:block;font-weight:600;color:#212b32;margin-top:0;font-size:18px;line-height:1.125}@media (min-width: 40.0625em){#nhsuk-cookie-banner h2{margin-bottom:24px}}@media (min-width: 40.0625em){#nhsuk-cookie-banner h2{font-size:22px;line-height:1.375}}#nhsuk-cookie-banner p{margin-bottom:16px;display:block;margin-top:0;font-size:16px;line-height:1.5}@media (min-width: 40.0625em){#nhsuk-cookie-banner p{margin-bottom:24px}}@media (min-width: 40.0625em){#nhsuk-cookie-banner p{font-size:19px;line-height:1.47368}}#nhsuk-cookie-banner a{font-weight:normal}#nhsuk-cookie-banner .nhsuk-button{margin-bottom:12px;display:inline-block;vertical-align:top;font-family:inherit;font-size:16px;line-height:1.5}@media (min-width: 40.0625em){#nhsuk-cookie-banner .nhsuk-button{margin-bottom:12px}}@media (min-width: 40.0625em){#nhsuk-cookie-banner .nhsuk-button{font-size:19px;line-height:1.47368}}#nhsuk-cookie-banner ul,#nhsuk-cookie-banner li{list-style:none;padding:0;margin:0}@media (min-width: 48.0625em){#nhsuk-cookie-banner li{display:inline;margin-right:20px}}#nhsuk-cookie-banner .nhsuk-success-banner{background-color:#007f3b;color:#fff;padding:8px 0 8px 0;position:relative}#nhsuk-cookie-banner .nhsuk-success-banner p{margin:0}#nhsuk-cookie-banner .nhsuk-success-banner a,#nhsuk-cookie-banner .nhsuk-success-banner a:visited{color:#fff}#nhsuk-cookie-banner .nhsuk-success-banner a:hover,#nhsuk-cookie-banner .nhsuk-success-banner a:focus{color:#212b32}#nhsuk-cookie-banner p{padding:0px;font-size:16px}@media (min-width: 40.0625em){#nhsuk-cookie-banner p{font-size:19px}}#nhsuk-cookie-banner a{text-decoration:underline;font-size:16px}@media (min-width: 40.0625em){#nhsuk-cookie-banner a{font-size:19px}}#nhsuk-cookie-banner a:hover{text-decoration:none}\n</style>';
   *   }
   */
  document.body.insertBefore(div, document.body.firstChild);
  hitLoggingUrl('seen');

  document.getElementById('nhsuk-cookie-banner__link_accept').addEventListener('click', (e) => {
    e.preventDefault();
    hitLoggingUrl('declined');
    handleLinkClick(onAccept);
  });

  document.getElementById('nhsuk-cookie-banner__link_accept_analytics').addEventListener('click', (e) => {
    e.preventDefault();
    hitLoggingUrl('accepted');
    handleLinkClick(onAnalyticsAccept);
  });
}

//--------------------------------------------------------------------------
// enable.js
//--------------------------------------------------------------------------
/*
 * Changing the type to text/javascript will not cause the script to execute.
 * We need to add a sibling and then remove the original node.
 */
function enableScript(script) {
  const newScript = document.createElement('script');
  newScript.text = script.text;
  const parent = script.parentElement;
  newScript.setAttribute('type', 'text/javascript');
  const src = script.getAttribute('src');
  if (src) {
    newScript.setAttribute('src', src);
  }
  parent.insertBefore(newScript, script);
  parent.removeChild(script);
}

/*
 * Enable iframes by setting the src from the data-src attribute
 */
function enableIframe(iframe) {
  const src = iframe.getAttribute('data-src');
  iframe.setAttribute('src', src);
}

/*
 * Should a script or iframe be enabled, given it has a cookieconsent attribute
 * value `cookieConsentAttribute` and the allowed categories are `allowedCategories`
 * Returns true or false
 */
function shouldEnable(allowedCategories, cookieConsentAttribute) {
  const cookieConsentTypes = cookieConsentAttribute.split(',');
  for (let i = 0; i < cookieConsentTypes.length; i++) {
    if (allowedCategories.indexOf(cookieConsentTypes[i]) === -1) {
      // If *any* of the cookieConsentTypes are not in the allowedCategories array, return false.
      return false;
    }
  }
  return true;
}

/**
 * Enable all scripts for a given data-cookieconsent category
 */
function enableScriptsByCategories(categories) {
  const scripts = document.querySelectorAll('script[data-cookieconsent]');
  // Do not use scripts.forEach due to poor browser support with NodeList.forEach
  for (let i = 0; i < scripts.length; i++) {
    const cookieconsent = scripts[i].getAttribute('data-cookieconsent');
    if (shouldEnable(categories, cookieconsent)) {
      enableScript(scripts[i]);
    }
  }
}

/**
 * Enable all iframes for a given data-cookieconsent category
 */
function enableIframesByCategories(categories) {
  const iframes = document.querySelectorAll('iframe[data-cookieconsent]');
  // Do not use iframes.forEach due to poor browser support with NodeList.forEach
  for (let i = 0; i < iframes.length; i++) {
    const cookieconsent = iframes[i].getAttribute('data-cookieconsent');
    if (shouldEnable(categories, cookieconsent)) {
      enableIframe(iframes[i]);
    }
  }
}

//--------------------------------------------------------------------------
// settings.js
//--------------------------------------------------------------------------

const scriptTag = document.currentScript;

// get properties from the scriptTag for the policy URL
function getPolicyUrl() {
  let dataPolicyUrl = '/our-policies/cookies-policy/';


  if (!scriptTag) {
    return dataPolicyUrl;
  }

  if (scriptTag.getAttribute('data-policy-url')) {
    dataPolicyUrl = scriptTag.getAttribute('data-policy-url');
  }

  return dataPolicyUrl;
}

/**
 * Ideally we would use the URL API here, but IE support is lacking.
 */
function makeUrlAbsolute(url) {
  // Setting and immediately getting the href attribute of an <a> tag might look a bit strange, but
  // the <a> will convert our possibly-relative URL to a definitely-absolute one for us.
  const link = document.createElement('a');
  link.href = url;
  return link.href;
}

// get properties from the scriptTag for noBanner
function getNoBanner() {

  const defaults = false;

  if (!scriptTag) {
    return defaults;
  }

  const dataNoBanner = scriptTag.getAttribute('data-nobanner');

  // overwrite the default settings with attributes found on the <script> tag
  if (dataNoBanner === 'true' || dataNoBanner === '') {
    return true;
  }

  return defaults;
}
