package uk.nhs.nhsbsa.prescriptions.consent.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.xpath;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import uk.nhs.nhsbsa.prescriptions.spring.cookie.consent.controller.test.CookieConsentExampleController;
@WebMvcTest(controllers = CookieConsentExampleController.class)
@AutoConfigureMockMvc
public class CookieConsentExampleControllerIntegrationTest {

  @Autowired
  MockMvc mockMvc;

  @Test
  @DisplayName("Should verify  cookie consent example title is Cookie Consent test page")
  public void loginPageTitle() throws Exception {
    mockMvc
        .perform(MockMvcRequestBuilders.get("/cookie-consent/test"))
        .andExpect(xpath("//title").string("Cookie Consent test page"))
        .andExpect(xpath("//*[@id=\"nhsuk-cookie-banner__link_accept_analytics\"]/text()").string("\n"
            + "                                I'm OK with analytics cookies\n"
            + "                            "))
        .andExpect(xpath("//*[@id=\"nhsuk-cookie-banner__link\"]").string("\n"
            + "                            read more about our cookies\n"
            + "                        "));
  }

}
