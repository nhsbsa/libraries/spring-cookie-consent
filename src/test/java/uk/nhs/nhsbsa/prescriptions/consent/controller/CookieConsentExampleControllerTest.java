package uk.nhs.nhsbsa.prescriptions.consent.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import uk.nhs.nhsbsa.prescriptions.spring.cookie.consent.controller.test.CookieConsentExampleController;

public class CookieConsentExampleControllerTest {
  private CookieConsentExampleController cookieConsentExampleController;

  @BeforeEach
  void setup() {
    cookieConsentExampleController = new CookieConsentExampleController();
  }

  @Test
  @DisplayName("Should verify cookie consent example view name is test")
  void shouldReturnTheCookieConsentExampleView() {
    //When
    String view = cookieConsentExampleController.test();

    // Then
    assertThat(view, is("cookie-consent-example/test"));
  }

}
